import React from 'react';
import {Link, NavLink, Router} from 'react-router-dom';
// import { createBrowserHistory } from 'history';
import styled from 'styled-components';
import Loading from './Loading';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

const MainColumn = styled.div`
  max-width: 1150px;
  margin: 0 auto;
`;

// const defaultHistory = createBrowserHistory();

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemList: [],
      loading: true,
      error: false,
      columnDefs: [
        {
          field: 'id',
          cellRenderer: (params) => {
            return <nav><NavLink to={`/item/${params.value}`}>{params.value}</NavLink></nav>
          }
        },
        { field: 'mtlNo' },
        { field: 'netWeight' },
        { field: 'orderDate' },
        { field: 'grade' },
        { field: 'supplyDate' },
        { field: 'locate' }
      ],
      onCellClicked: (event) => {
        if (event && event.column && event.column.field === "id") {
          props.history.push(`/item/${event.value}`);
        }
      }
    };
  }

  componentDidMount() {
    const host = process.env.REACT_APP_CONTENT_HOST;
    // console.log(this.props.history);
    fetch(`${host}/itemList.json`)
      .then(result => result.json())
      .then(itemList => {
        this.setState({
          itemList: itemList.map(item => ({
            ...item
          })),
          loading: false,
        });
      })
      .catch(() => {
        this.setState({ loading: false, error: true });
      });
  }

  render() {
    const {
      itemList,
      loading,
      error,
      columnDefs
    } = this.state;

    if (loading) {
      return <Loading />;
    }

    if (error) {
      return (
        <MainColumn>
          Sorry, but the restaurant list is unavailable right now
        </MainColumn>
      );
    }

    return (
        <Router history={this.props.history}>

          <MainColumn>
            <div className="ag-theme-alpine" style={{height: 400, width: 600}}>
              <AgGridReact
                  rowData={itemList}
                  columnDefs={columnDefs}>
              </AgGridReact>
            </div>
          </MainColumn>
        </Router>
    );
  }
}

export default App;
