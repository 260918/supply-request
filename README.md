# Hot coil list for supply-request

[![Build Status](https://travis-ci.org/micro-frontends-demo/browse.svg?branch=master)](https://travis-ci.org/micro-frontends-demo/browse)

A micro frontend for supply-request.

# Getting started

1. Clone the repo
2. `yarn install`
3. `yarn start`

You'll also need to run the [`content`](https://gitlab.com/260918/content) server.

This will run the app on its own, outside of the container. This is useful while
you're tweaking logic and styling, but you'll usually then need to integrate and
test it with the rest of the application. So you should also run:

- the [`container`](https://gitlab.com/260918/container) application
- the [`yard`](https://gitlab.com/260918/yard) micro frontend

